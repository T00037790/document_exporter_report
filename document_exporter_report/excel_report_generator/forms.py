from .models import Customer
from django import forms


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer

        fields = [
            'name',
        ]
        labels = {
            'name': 'Name',
        }
        widgets = {
            'name': forms.Select(),
        }
