from django.apps import AppConfig


class ExcelReportGeneratorConfig(AppConfig):
    name = 'excel_report_generator'

