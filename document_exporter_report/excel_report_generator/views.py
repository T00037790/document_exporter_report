from django.http import Http404, HttpResponse
from django.shortcuts import render
from django.views.generic.base import View
from .models import Customer
import logging
import datetime
import os
from django.conf import settings
from .tasks import create_excel
from celery import signature


class Index(View):

    @staticmethod
    def get(request):
        customer = Customer.objects.all()
        context = {'customer': customer}
        return render(request, 'document_exporter_report/index.html', context)

    @staticmethod
    def post(request):
        customer = request.POST['customer']
        filter = request.POST['filter']
        try:
            checkbox = request.POST['checkbox']
        except Exception as e:
            checkbox = 'off'
            logging.debug("error", e)
        date_range = request.POST['date_range']
        data_split = date_range.split(" - ")
        initial_date_string = data_split[0]
        initial = datetime.datetime.strptime(initial_date_string, '%m/%d/%Y')
        finished_date_string = data_split[1]
        finish = datetime.datetime.strptime(finished_date_string, '%m/%d/%Y')
        file_path = create_excel.s(customer, filter, initial, finish, checkbox)
        context = {'file_path': file_path}
        return render(request, 'document_exporter_report/download.html', context)


def success(request, filepath):
    context = {}
    filepath = 'Reports/' + filepath
    file_path = os.path.join(settings.MEDIA_ROOT, filepath)
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            # response['Refresh'] = "0;url=/success"
            return response
    raise Http404

