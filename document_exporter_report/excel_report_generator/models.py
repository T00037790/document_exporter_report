from django.db import models
from datetime import datetime
import os


class Customer(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30)
    PARSERS_CHOICES = (
        (1, 'Parser 1'),
        (2, 'Parser 2'),
        (3, 'Parser 3'),
        (4, 'Parser 4'),
        (5, 'Parser 5'),
        (6, 'Parser 6'),
        (7, 'Parser 7'),
        (8, 'Parser 8'),
        (9, 'Parser 9'),
        (10, 'Parser 10'),
    )
    parser = models.IntegerField(choices=PARSERS_CHOICES)

    def __str__(self):
        return '{}'.format(self.name)


def file_path_and_name(instance, filename):
    base_file_name, file_extension = os.path.splitext(filename)
    _now = datetime.now()
    return 'uploads/{basename} {date} {time} {ext}'.format(
        basename=base_file_name, ext=file_extension,
        date=_now.strftime('%Y-%m-%d'), time=_now.strftime('%H:%M:%S')
    )


class FileParser(models.Model):
    id = models.AutoField(primary_key=True)
    email_id = models.IntegerField()
    customer_id = models.ForeignKey(Customer, on_delete=models.CASCADE)
    email_date = models.DateTimeField(auto_now=False, auto_now_add=False)
    parser = models.TextField()
    OPTIONS = (
        (100, 'by email'),
        (200, 'uploaded by user'),
    )
    options = models.IntegerField(choices=OPTIONS)
    sent = models.BooleanField()
    file = models.FileField(upload_to=file_path_and_name)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}'.format(self.email_date)




