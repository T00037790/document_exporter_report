import datetime
from .models import Customer, FileParser
import xlsxwriter
import os
import pytz
from document_exporter_report.celery import app


@app.task
def create_excel(customer, filter, initial, finish, checkbox):
    _now = datetime.datetime.now()
    date = _now.strftime('%Y-%m-%d %H:%M:%S')
    file_name = 'Report ' + str(date)
    object = Customer.objects.get(name=customer)
    name = object.name
    parser = object.parser
    file_parser_object = FileParser.objects.filter(customer_id=object.id)

    if not os.path.exists('Reports'):
        os.makedirs('Reports')
    workbook = xlsxwriter.Workbook('Reports/' + file_name + '.xlsx')
    worksheet = workbook.add_worksheet("Report")
    worksheet.write('A1', 'Name')
    worksheet.write('B1', 'Parser')
    worksheet.write('C1', 'Email ID')
    worksheet.write('D1', 'Email Date')
    worksheet.write('E1', 'Parse')
    worksheet.write('F1', 'Option')
    worksheet.write('G1', 'Sent')
    worksheet.write('H1', 'File')
    worksheet.write('I1', 'Created at')
    count = 2
    utc = pytz.UTC
    initial = utc.localize(initial)
    finish = utc.localize(finish)
    if checkbox == 'on':
        checkbox = 1
    else:
        checkbox = 0
    for parse in file_parser_object:
        row = str(count)
        if filter == 'Email_date':
            if initial <= parse.email_date <= finish:
                if checkbox == parse.sent:
                    worksheet.write('A' + row, name)
                    if parser == 1:
                        worksheet.write('B' + row, 'Parser 1')
                    elif parser == 2:
                        worksheet.write('B' + row, 'Parser 2')
                    elif parser == 3:
                        worksheet.write('B' + row, 'Parser 3')
                    elif parser == 4:
                        worksheet.write('B' + row, 'Parser 4')
                    elif parser == 5:
                        worksheet.write('B' + row, 'Parser 5')
                    worksheet.write('C' + row, parse.email_id)
                    worksheet.write('D' + row, str(parse.email_date.date().strftime("%d %B, %Y")))
                    worksheet.write('E' + row, parse.parser)
                    worksheet.write('F' + row, 'by email') if (parse.options == 100) else \
                        worksheet.write('F' + row, 'uploaded by user')
                    if parse.sent == 0:
                        worksheet.write('G' + row, 'No')
                    else:
                        worksheet.write('G' + row, 'Yes')
                    worksheet.write('H' + str(count), parse.file.url)
                    worksheet.write('I' + row, str(parse.created_at.date().strftime("%d %B, %Y")))
                    count += 1
        elif filter == 'Creation_Date':
            if initial <= parse.created_at <= finish:
                if checkbox == parse.sent:
                    worksheet.write('A' + row, name)
                    if parser == 1:
                        worksheet.write('B' + row, 'Parser 1')
                    elif parser == 2:
                        worksheet.write('B' + row, 'Parser 2')
                    elif parser == 3:
                        worksheet.write('B' + row, 'Parser 3')
                    elif parser == 4:
                        worksheet.write('B' + row, 'Parser 4')
                    elif parser == 5:
                        worksheet.write('B' + row, 'Parser 5')
                    worksheet.write('C' + row, parse.email_id)
                    worksheet.write('D' + row, str(parse.email_date.date().strftime("%d %B, %Y")))
                    worksheet.write('E' + row, parse.parser)
                    worksheet.write('F' + row, 'by email') if (parse.options == 100) else \
                        worksheet.write('F' + row, 'uploaded by user')
                    if parse.sent == 0:
                        worksheet.write('G' + row, 'No')
                    else:
                        worksheet.write('G' + row, 'Yes')
                    worksheet.write('H' + str(count), parse.file.url)
                    worksheet.write('I' + row, str(parse.created_at.date().strftime("%d %B, %Y")))
                    count += 1

    workbook.close()
    return '{}.xlsx'.format(file_name)
