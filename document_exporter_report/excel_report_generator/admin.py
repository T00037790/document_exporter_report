from django.contrib import admin

from .models import Customer, FileParser


class AuthorAdmin(admin.ModelAdmin):
    pass


admin.site.register(Customer, AuthorAdmin)


@admin.register(FileParser)
class FileParser(admin.ModelAdmin):
    pass
