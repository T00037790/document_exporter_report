from django.urls import path
from .views import Index, success

app_name = 'excel_report_generator'

urlpatterns = [
    path('', Index.as_view(), name='index'),
    path('success/<filepath>/', success, name='success')
]
