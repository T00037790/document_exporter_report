import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'document_exporter_report.settings')


app = Celery('document_exporter_report')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
